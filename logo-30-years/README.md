# Logo 30 years Debian

All artwork in this repository has been produced collaboratively and is
licensed in a way that allows such reproduction.

Font: yanone kaffeesatz

## Author:

* Jefferson Maier ( maierjefferson at gmail com )

## License:

[Creative Commons Attribution Share Alike 4.0](https://salsa.debian.org/debconf-team/public/data/dc19/blob/master/LICENSE.txt)

